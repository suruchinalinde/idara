webpackJsonp([14],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnnualCalendarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AnnualCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AnnualCalendarPage = /** @class */ (function () {
    function AnnualCalendarPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AnnualCalendarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AnnualCalendarPage');
    };
    AnnualCalendarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-annual-calendar',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\annual-calendar\annual-calendar.html"*/'<!--\n  Generated template for the AnnualCalendarPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>AnnualCalendar</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\annual-calendar\annual-calendar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AnnualCalendarPage);
    return AnnualCalendarPage;
}());

//# sourceMappingURL=annual-calendar.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttendancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AttendancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AttendancePage = /** @class */ (function () {
    function AttendancePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AttendancePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AttendancePage');
    };
    AttendancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-attendance',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\attendance\attendance.html"*/'<!--\n  Generated template for the AttendancePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Attendance</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\attendance\attendance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AttendancePage);
    return AttendancePage;
}());

//# sourceMappingURL=attendance.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChangePasswordPage = /** @class */ (function () {
    function ChangePasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChangePasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChangePasswordPage');
    };
    ChangePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-change-password',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\change-password\change-password.html"*/'<ion-header>\n\n    <ion-navbar class="bkcolor">\n        <ion-grid class="bkcolor">\n          <ion-row>\n           <ion-col col-12 class="mm">\n                  <div class="center">\n                        <ion-label class="idarafont">Change Password</ion-label>\n                  </div>\n                </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-navbar>\n  </ion-header>\n\n\n<ion-content padding>\n   \n    <img class="img1" src="assets/icon/key.png">\n  \n  <ion-grid>\n    <div>\n    <ion-row>   \n            <label for="current">Current Password :</label>   \n          </ion-row>\n          <ion-row>\n            <ion-input type="text" name="current" required class="inputlabelmm"></ion-input>  \n          </ion-row>\n        </div>\n\n\n        <div>\n            <ion-row  class="topmg">   \n                    <label for="current">New Password :</label>   \n                  </ion-row>\n                  <ion-row>\n                    <ion-input type="text" name="current" required class="inputlabelmm"></ion-input>  \n                  </ion-row>\n                </div>\n    \n                <div class="topmg">\n                    <ion-row>   \n                            <label for="current">Confirm Password :</label>   \n                          </ion-row>\n                          <ion-row>\n                            <ion-input type="text" name="current" required class="inputlabelmm"></ion-input>  \n                          </ion-row>\n                        </div>\n      \n  \n    <div padding>\n      <button ion-button class="buttoncss">Submit</button>\n    </div>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\change-password\change-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ChangePasswordPage);
    return ChangePasswordPage;
}());

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FeedbackPage = /** @class */ (function () {
    function FeedbackPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FeedbackPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedbackPage');
    };
    FeedbackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-feedback',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\feedback\feedback.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Write To School</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n    <div padding>    \n      <label>Name :</label>  <br>  \n      <input type="text" name="name" required class="inputlabel">  \n    </div>\n    <div padding>    \n      <label>Message</label>    <br>\n      <textarea name="message" rows="4" cols="100%" required class="inputlabel"> </textarea> \n    </div>\n    <div padding>\n        <button ion-button style="float: right;background-color: #ff9100">Send</button>\n    </div>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\feedback\feedback.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], FeedbackPage);
    return FeedbackPage;
}());

//# sourceMappingURL=feedback.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeworkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomeworkPage = /** @class */ (function () {
    function HomeworkPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HomeworkPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomeworkPage');
    };
    HomeworkPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homework',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\homework\homework.html"*/'<!--\n  Generated template for the HomeworkPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Home Work</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row class="MPView">\n      <ion-col>\n        <ion-label class="labelFont">Title :<ion-note> Science Project</ion-note></ion-label>        \n      </ion-col>\n    </ion-row>\n    <ion-row class="MPView">\n        <ion-col class="labelFont">\n        <ion-label>Description :</ion-label>     \n        <ion-note>A science fair experiment is generallya competition where constant present their science project, results in the form of a report, display board, and /or models that they have created. Science fairs allows Students in elementry.</ion-note>   \n      </ion-col>\n    </ion-row>\n    <ion-row class="MPView">\n      <ion-col class="labelFont">\n        <ion-label>Task :</ion-label>\n        <ion-note>Conduct your experiment and report your results.</ion-note>        \n      </ion-col>\n    </ion-row>\n    <ion-row class="MPView">\n      <ion-col class="labelFont">\n        <ion-label>Start Date :</ion-label>\n        <ion-note>2nd Feb 2018</ion-note>        \n      </ion-col>\n      <ion-col class="labelFont">\n        <ion-label>End Date :</ion-label>\n        <ion-note>2nd Feb 2018</ion-note>        \n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <img src="assets/imgs/pdfimg.png" width="150px" height="200px">       \n      </ion-col>\n    </ion-row>  \n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\homework\homework.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], HomeworkPage);
    return HomeworkPage;
}());

//# sourceMappingURL=homework.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\profile\profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row col-12 class="borderbottom">  \n      <ion-col col-6 class="borderright">          \n          <img  class="imgp" src="assets/icon/user.png">          \n          <ion-label class="namelabel">Student Name</ion-label>            \n      </ion-col>     \n      <ion-col col-6>         \n        <ion-row>\n          <ion-label class="labelmargin">ADMISSION NO.</ion-label>    \n        </ion-row>\n        <ion-row>\n          <ion-note class="notemargin">785</ion-note>\n        </ion-row>         \n        <ion-row>\n          <ion-label class="labelmargin">CLASS-SECTION</ion-label>  \n        </ion-row>\n        <ion-row>    \n          <ion-note class="notemargin">9-B</ion-note>\n        </ion-row>\n        <ion-row>\n          <ion-label class="labelmargin">CLASS TEACHER</ion-label>    \n        </ion-row>\n        <ion-row>  \n          <ion-note class="notemargin">Suruchi Nalinde mam</ion-note>\n        </ion-row>       \n      </ion-col>\n    </ion-row>\n   \n    <ion-list class="listmargin">\n      <ion-row>     \n        <ion-col col-2> \n          <img class="imgsize" src="assets/icon/student (1).png">    \n        </ion-col>  \n        <ion-col col-10> \n          <ion-label for="fname">Mr.John</ion-label>           \n         </ion-col>  \n      </ion-row>\n      <ion-row>      \n        <ion-col col-2> \n          <img class="imgsize" src="assets/icon/girl (2).png">\n        </ion-col>  \n        <ion-col col-10> \n          <ion-label for="mname">Mrs. Kiyara</ion-label>           \n        </ion-col>     \n      </ion-row>\n      <ion-row>     \n        <ion-col col-2>        \n          <img class="imgsize" src="assets/icon/phone (1).png">         \n        </ion-col>    \n        <ion-col col-10>        \n          <ion-label for="mobno">7744032651</ion-label>           \n        </ion-col>      \n      </ion-row>\n      <ion-row>      \n        <ion-col col-2>      \n          <img class="imgsize" src="assets/icon/envelope.png">         \n        </ion-col>   \n        <ion-col col-10>      \n          <ion-label for="gmail">sample00@gmail.com</ion-label>           \n        </ion-col>     \n      </ion-row>\n      <ion-row>     \n        <ion-col col-2>       \n          <img class="imgsize" src="assets/icon/house.png">       \n        </ion-col>  \n        <ion-col col-10>       \n          <ion-label for="address" text-wrap>Department 98, 44-46 Morning Side Road Edinburgh Scotland</ion-label>           \n        </ion-col>     \n      </ion-row>\n    </ion-list>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"D:\ionic\Idara\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResultPage = /** @class */ (function () {
    function ResultPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ResultPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultPage');
    };
    ResultPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-result',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\result\result.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Exam Result</ion-title>\n  </ion-navbar>\n\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="style3">\n    <ion-row class="rowcolor">\n      <ion-col col-6>\n        <ion-item class="style2">\n\n          <ion-label class="style1">Exam</ion-label>\n        </ion-item>\n      </ion-col>\n      <ion-col col-6>\n        <ion-item class="stylem">\n          <ion-select interface="popover">\n            <ion-option value="first">First Exam</ion-option>\n            <ion-option value="second">Second Exam</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <div class="tablediv">\n    <table>\n      <tr>\n        <th>Suject</th>\n        <th>Marks</th>\n        <th>Gread</th>\n        <th>Comment</th>\n      </tr>\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n      <tr>\n        <td>Math</td>\n        <td>85</td>\n        <td>A</td>\n        <td>Very Good</td>\n      </tr>\n\n    </table>\n  </div>\n\n  <ion-grid>\n    <ion-row class="rowtop">\n      <ion-col col-4>\n        <div class="mm">\n          <h2 class="colround">521/800</h2>\n        </div>\n      </ion-col>\n\n      <ion-col col-4>\n        <div class="mm">\n          <h2 class="colround">GPA 0.0</h2>\n        </div>\n      </ion-col>\n\n      <ion-col col-4>\n        <div class="mm">\n          <h2 class="pass">Pass</h2>\n        </div>\n      </ion-col>\n    </ion-row>\n\n\n  </ion-grid>\n  <div class="noteresult">\n    <ion-note> GPA: Great point Average</ion-note>\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\ionic\Idara\src\pages\result\result.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ResultPage);
    return ResultPage;
}());

//# sourceMappingURL=result.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TransportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TransportPage = /** @class */ (function () {
    function TransportPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TransportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TransportPage');
    };
    TransportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-transport',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\transport\transport.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Transport</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n    <img src="assets/icon/school-bus.png" class="imgsize">\n  </div>\n  <div>  \n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-label>Route Name :\n            <ion-note>City Centre</ion-note>\n          </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label>No. of Vehicle :\n            <ion-note>321</ion-note>\n          </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label>Vehicle Registered No. :\n            <ion-note>MH 1234</ion-note>\n          </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label>Driver Name :\n            <ion-note>John Stewert</ion-note>\n          </ion-label> \n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label>Driver Phone No. :\n            <ion-note>9876543210</ion-note>\n          </ion-label>\n        </ion-col>\n      </ion-row>\n    </ion-grid>      \n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\transport\transport.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], TransportPage);
    return TransportPage;
}());

//# sourceMappingURL=transport.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticeBoardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NoticeBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NoticeBoardPage = /** @class */ (function () {
    function NoticeBoardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NoticeBoardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NoticeBoardPage');
    };
    NoticeBoardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notice-board',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\notice-board\notice-board.html"*/'<!--\n  Generated template for the NoticeBoardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Notice Board</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-label class="labelStyle">Parents Meeting</ion-label>\n      </ion-row>\n      <ion-row>\n        <ion-note class="fontStyle">\n          Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n          Lorem Ipsum has been the industriy\'s standard dummy text ever since the 1500s, \n          when an unknown  printer took a gallery of type and scrambled it to make a type \n          specimen book. It has serview not only few countries, but also the leap into \n          electronic type setting, remaining esseentially unchanged. It was popularised in\n          the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, \n          and more recently with desktop publishing software like Aldus Page Maker\n          including versions of Lorem Ipsum. \n        </ion-note>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\notice-board\notice-board.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], NoticeBoardPage);
    return NoticeBoardPage;
}());

//# sourceMappingURL=notice-board.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimetableweekPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TimetableweekPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TimetableweekPage = /** @class */ (function () {
    function TimetableweekPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TimetableweekPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TimetableweekPage');
    };
    TimetableweekPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-timetableweek',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\timetableweek\timetableweek.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Time Table</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n\n    <div class="divrow">\n\n   \n    <ion-row>\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n           <ion-label>MONDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">MON</h2>\n        </ion-card>\n      </ion-col>\n\n\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n            <ion-label> TUESDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">TUE</h2>\n        </ion-card>\n      </ion-col>\n\n\n    </ion-row>\n  </div>\n\n\n<div class="divrow">\n    <ion-row>\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n            <ion-label>  WEDNESDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">WED</h2>\n        </ion-card>\n      </ion-col>\n\n\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n            <ion-label> THURSDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">THUR</h2>\n        </ion-card>\n      </ion-col>\n\n      \n    </ion-row>\n  </div>\n\n<div class="divrow">\n    <ion-row>\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n            <ion-label>  FRIDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">FRI</h2>\n        </ion-card>\n      </ion-col>\n\n\n      <ion-col col-6>\n        <ion-card class="maincard">\n          <ion-card-header class="cardhead">\n            <ion-label> SATURDAY</ion-label> \n          </ion-card-header>\n          <h2 class="h2set">SAT</h2>\n        </ion-card>\n      </ion-col>\n\n      \n    </ion-row>\n  </div>\n    \n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"D:\ionic\Idara\src\pages\timetableweek\timetableweek.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], TimetableweekPage);
    return TimetableweekPage;
}());

//# sourceMappingURL=timetableweek.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/annual-calendar/annual-calendar.module": [
		283,
		13
	],
	"../pages/attendance/attendance.module": [
		284,
		12
	],
	"../pages/change-password/change-password.module": [
		285,
		11
	],
	"../pages/day-time-table/day-time-table.module": [
		286,
		10
	],
	"../pages/events/events.module": [
		287,
		9
	],
	"../pages/feedback/feedback.module": [
		288,
		8
	],
	"../pages/homework/homework.module": [
		289,
		7
	],
	"../pages/login/login.module": [
		290,
		6
	],
	"../pages/notice-board/notice-board.module": [
		291,
		5
	],
	"../pages/profile/profile.module": [
		292,
		4
	],
	"../pages/result/result.module": [
		293,
		3
	],
	"../pages/select-student/select-student.module": [
		294,
		2
	],
	"../pages/timetableweek/timetableweek.module": [
		295,
		1
	],
	"../pages/transport/transport.module": [
		296,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_profile__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__attendance_attendance__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__homework_homework__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__result_result__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__transport_transport__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notice_board_notice_board__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__feedback_feedback__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__annual_calendar_annual_calendar__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__select_student_select_student__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__change_password_change_password__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__login_login__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__timetableweek_timetableweek__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.profile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__profile_profile__["a" /* ProfilePage */]);
    };
    HomePage.prototype.attendance = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__attendance_attendance__["a" /* AttendancePage */]);
    };
    HomePage.prototype.homework = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__homework_homework__["a" /* HomeworkPage */]);
    };
    HomePage.prototype.marks = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__result_result__["a" /* ResultPage */]);
    };
    HomePage.prototype.buslocation = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__transport_transport__["a" /* TransportPage */]);
    };
    HomePage.prototype.noticeboard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__notice_board_notice_board__["a" /* NoticeBoardPage */]);
    };
    HomePage.prototype.writetoschool = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__feedback_feedback__["a" /* FeedbackPage */]);
    };
    HomePage.prototype.timeTable = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__timetableweek_timetableweek__["a" /* TimetableweekPage */]);
    };
    HomePage.prototype.annualcalender = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__annual_calendar_annual_calendar__["a" /* AnnualCalendarPage */]);
    };
    HomePage.prototype.SelectPage = function (menu) {
        console.log(menu);
        if (menu == 1) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__select_student_select_student__["a" /* SelectStudentPage */]);
        }
        else if (menu == 2) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__change_password_change_password__["a" /* ChangePasswordPage */]);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__login_login__["a" /* LoginPage */]);
        }
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\home\home.html"*/'<ion-header>\n  \n  <ion-navbar class="bkcolor">\n      <ion-grid class="bkcolor">\n        <ion-row>\n\n\n            <ion-col col-10 class="mm">\n                <div class="center">\n                  <!-- <div class="div-1"> -->\n                      <ion-label class="idarafont">Idara MSB</ion-label>\n    \n                  <!-- </div> -->\n                </div>\n              </ion-col>\n\n\n          <ion-col col-2 class="col-1">\n  \n            <!-- <ion-buttons> -->\n              <!-- <button ion-button class="button-inner" > -->\n                <ion-icon name="ios-settings" class="backicon" >\n                    <ion-select interface="popover" (ionChange)="SelectPage($event)" [(ngModel)]="menu" >\n                        <ion-option value="1"><img src="assets/icon/key2.png" width="57" height="35"/>Select Student</ion-option>\n                        <ion-option value="2"><ion-icon name="stats"></ion-icon>Change Password</ion-option>\n                        <ion-option value="3">LogOut</ion-option>\n                         </ion-select>\n                          </ion-icon>\n              <!-- </button> -->\n            <!-- </ion-buttons> -->\n  \n          </ion-col>\n  \n  \n  \n  \n        </ion-row>\n      </ion-grid>\n  \n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid  class="gridviewhome">\n    <ion-row>\n\n      <ion-col col-6>\n        <ion-item (click)="profile()">\n          <ion-avatar>\n          <img class="img123" src="assets/icon/student.png">\n          </ion-avatar>\n          <label>Profile</label>\n                 \n        </ion-item>\n      </ion-col>\n\n      <ion-col col-6>\n        <ion-item (click)="attendance()">\n          <ion-avatar>\n          <img class="img123" src="assets/icon/clock.png">\n          </ion-avatar>\n          <label>Attendance</label>\n        \n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n    <ion-row>\n\n      <ion-col col-6>\n        <ion-item (click)="homework()">\n          <ion-avatar>\n            <img class="img123" src="assets/icon/homework.png">\n          </ion-avatar>\n          <label>Home Work</label>\n          \n        </ion-item>\n      </ion-col>\n  \n      <ion-col col-6>\n        <ion-item (click)="marks()">\n          <ion-avatar>\n              <img class="img123" src="assets/icon/medal (1).png">\n            </ion-avatar>\n            <label>Marks</label>                  \n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n    <ion-row>\n\n      <ion-col col-6>\n        <ion-item  (click)="buslocation()">\n          <ion-avatar>\n              <img class="img123" src="assets/icon/placeholder.png">\n            </ion-avatar>\n            <label>Bus Location</label>\n        </ion-item>\n      </ion-col>\n    \n      <ion-col col-6>\n        <ion-item (click)="noticeboard()">\n          <ion-avatar>\n              <img class="img123" src="assets/icon/board.png">\n            </ion-avatar>\n            <label>Notice Board</label>          \n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n    <ion-row>\n\n      <ion-col col-6>\n        <ion-item (click)="writetoschool()">\n          <ion-avatar>\n              <img class="img123" src="assets/icon/document.png">\n            </ion-avatar>\n            <label>Write to School</label>\n          \n        </ion-item>\n      </ion-col>\n    \n      <ion-col col-6>\n        <ion-item (click)="timeTable()">\n          <ion-avatar>\n            <img class="img123" src="assets/icon/calendar.png">\n          </ion-avatar>\n          <label>Time Table</label>\n             \n        </ion-item>\n      </ion-col>\n      </ion-row>\n      <ion-row>\n\n     <ion-col col-6>\n          <ion-item (click)="annualcalender()">\n            <ion-avatar>\n                <img class="img123" src="assets/icon/time-table.png">\n              </ion-avatar>\n              <label>Anual Calender</label>            \n          </ion-item>\n        </ion-col>\n\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DayTimeTablePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DayTimeTablePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DayTimeTablePage = /** @class */ (function () {
    function DayTimeTablePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DayTimeTablePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DayTimeTablePage');
    };
    DayTimeTablePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-day-time-table',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\day-time-table\day-time-table.html"*/'<!--\n  Generated template for the DayTimeTablePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Monday</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row class="border">\n      <ion-col col-6>\n        <ion-label class="labelFont">Math</ion-label>\n      </ion-col>\n      <ion-col col-6 class="align">\n        <ion-note>Teachers Name</ion-note><br>\n        <ion-note>10:00 AM</ion-note>\n      </ion-col>\n    </ion-row>\n    <ion-row class="border">\n      <ion-col col-6>\n        <ion-label class="labelFont">English</ion-label>\n      </ion-col>\n      <ion-col col-6 class="align">\n        <ion-note>Teachers Name</ion-note><br>\n        <ion-note>10:00 AM</ion-note>\n      </ion-col>\n    </ion-row>\n    <ion-row class="border">\n      <ion-col col-6>\n        <ion-label class="labelFont">Hindi</ion-label>\n      </ion-col>\n      <ion-col col-6 class="align">\n        <ion-note>Teachers Name</ion-note><br>\n        <ion-note>10:00 AM</ion-note>\n      </ion-col>\n    </ion-row>\n    <ion-row class="border">\n      <ion-col col-6>\n        <ion-label class="labelFont">Science</ion-label>\n      </ion-col>\n      <ion-col col-6 class="align">\n        <ion-note>Teachers Name</ion-note><br>\n        <ion-note>10:00 AM</ion-note>\n      </ion-col>\n      </ion-row>\n        <ion-row class="border">\n          <ion-col col-6>\n            <ion-label class="labelFont">Grammer</ion-label>\n          </ion-col>\n          <ion-col col-6 class="align">\n            <ion-note>Teachers Name</ion-note><br>\n            <ion-note>10:00 AM</ion-note>\n          </ion-col>\n        </ion-row>\n        <ion-row class="border">\n          <ion-col col-6>\n            <ion-label class="labelFont">Sports</ion-label>\n          </ion-col>\n          <ion-col col-6 class="align">\n            <ion-note>Teachers Name</ion-note><br>\n            <ion-note>10:00 AM</ion-note>\n          </ion-col>\n        </ion-row>\n        <ion-row class="border">\n          <ion-col col-6>\n            <ion-label class="labelFont">Civics</ion-label>\n          </ion-col>\n          <ion-col col-6 class="align">\n            <ion-note>Teachers Name</ion-note><br>\n            <ion-note>10:00 AM</ion-note>\n          </ion-col>\n        </ion-row>\n        <ion-row class="border">\n          <ion-col col-6>\n            <ion-label class="labelFont">Geography</ion-label>\n          </ion-col>\n          <ion-col col-6 class="align">\n            <ion-note>Teachers Name</ion-note><br>\n            <ion-note>10:00 AM</ion-note>\n          </ion-col>\n        </ion-row>\n        <ion-row class="border">\n          <ion-col col-6>\n            <ion-label class="labelFont">Physics</ion-label>\n          </ion-col>\n          <ion-col col-6 class="align">\n            <ion-note>Teachers Name</ion-note><br>\n            <ion-note>10:00 AM</ion-note>\n          </ion-col>\n        </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\day-time-table\day-time-table.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], DayTimeTablePage);
    return DayTimeTablePage;
}());

//# sourceMappingURL=day-time-table.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventsPage = /** @class */ (function () {
    function EventsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EventsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventsPage');
    };
    EventsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-events',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\events\events.html"*/'<!--\n  Generated template for the EventsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Annual Event List</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-grid class="cardlayout">\n      <ion-row>     \n        <ion-col class="paddingMargin">\n            <ion-label class="titlefont paddingMargin">Sports day</ion-label>\n        </ion-col>   \n      </ion-row>  \n\n      <ion-row>     \n          <ion-col class="paddingMargin">     \n            <ion-note class="contentFont">\n          Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n             </ion-note>\n          </ion-col>\n      </ion-row>\n\n        \n      <ion-row>     \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin">Date :<ion-note class="contentFont"> 2nd Feb 2018</ion-note></p>                                 \n        </ion-col>\n           \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin floatRight">Time :<ion-note class="contentFont"> 10:00 PM</ion-note></p>              \n        </ion-col>\n      </ion-row>\n  </ion-grid>\n\n  <ion-grid class="cardlayout">\n      <ion-row>     \n        <ion-col class="paddingMargin">\n            <ion-label class="titlefont paddingMargin">Drawing Contest</ion-label>\n        </ion-col>   \n      </ion-row>  \n\n      <ion-row>     \n          <ion-col class="paddingMargin">     \n            <ion-note class="contentFont">\n          Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n             </ion-note>\n          </ion-col>\n      </ion-row>\n\n        \n      <ion-row>     \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin">Date :<ion-note class="contentFont"> 2nd Feb 2018</ion-note></p>                                 \n        </ion-col>\n           \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin floatRight">Time :<ion-note class="contentFont"> 10:00 PM</ion-note></p>              \n        </ion-col>\n      </ion-row>\n  </ion-grid>\n  <ion-grid class="cardlayout">\n      <ion-row>     \n        <ion-col class="paddingMargin">\n            <ion-label class="titlefont paddingMargin">Sports day</ion-label>\n        </ion-col>   \n      </ion-row>  \n\n      <ion-row>     \n          <ion-col class="paddingMargin">     \n            <ion-note class="contentFont">\n          Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n             </ion-note>\n          </ion-col>\n      </ion-row>\n\n        \n      <ion-row>     \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin">Date :<ion-note class="contentFont"> 2nd Feb 2018</ion-note></p>                                 \n        </ion-col>\n           \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin floatRight">Time :<ion-note class="contentFont"> 10:00 PM</ion-note></p>              \n        </ion-col>\n      </ion-row>\n  </ion-grid>\n  <ion-grid class="cardlayout">\n      <ion-row>     \n        <ion-col class="paddingMargin">\n            <ion-label class="titlefont paddingMargin">Sports day</ion-label>\n        </ion-col>   \n      </ion-row>  \n\n      <ion-row>     \n          <ion-col class="paddingMargin">     \n            <ion-note class="contentFont">\n          Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n             </ion-note>\n          </ion-col>\n      </ion-row>\n\n        \n      <ion-row>     \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin">Date :<ion-note class="contentFont"> 2nd Feb 2018</ion-note></p>                                 \n        </ion-col>\n           \n        <ion-col col-6 class="paddingMargin">\n          <p class="paddingMargin floatRight">Time :<ion-note class="contentFont"> 10:00 PM</ion-note></p>              \n        </ion-col>\n      </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\events\events.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], EventsPage);
    return EventsPage;
}());

//# sourceMappingURL=events.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(231);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_change_password_change_password__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_attendance_attendance__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_notice_board_notice_board__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_annual_calendar_annual_calendar__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_events_events__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_homework_homework__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_result_result__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_select_student_select_student__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_transport_transport__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_feedback_feedback__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_timetableweek_timetableweek__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_day_time_table_day_time_table__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_attendance_attendance__["a" /* AttendancePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_notice_board_notice_board__["a" /* NoticeBoardPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_annual_calendar_annual_calendar__["a" /* AnnualCalendarPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_events_events__["a" /* EventsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_homework_homework__["a" /* HomeworkPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_result_result__["a" /* ResultPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_select_student_select_student__["a" /* SelectStudentPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_transport_transport__["a" /* TransportPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_feedback_feedback__["a" /* FeedbackPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_timetableweek_timetableweek__["a" /* TimetableweekPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_day_time_table_day_time_table__["a" /* DayTimeTablePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/annual-calendar/annual-calendar.module#AnnualCalendarPageModule', name: 'AnnualCalendarPage', segment: 'annual-calendar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/attendance/attendance.module#AttendancePageModule', name: 'AttendancePage', segment: 'attendance', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/change-password/change-password.module#ChangePasswordPageModule', name: 'ChangePasswordPage', segment: 'change-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/day-time-table/day-time-table.module#DayTimeTablePageModule', name: 'DayTimeTablePage', segment: 'day-time-table', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/events/events.module#EventsPageModule', name: 'EventsPage', segment: 'events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/homework/homework.module#HomeworkPageModule', name: 'HomeworkPage', segment: 'homework', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notice-board/notice-board.module#NoticeBoardPageModule', name: 'NoticeBoardPage', segment: 'notice-board', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/result/result.module#ResultPageModule', name: 'ResultPage', segment: 'result', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/select-student/select-student.module#SelectStudentPageModule', name: 'SelectStudentPage', segment: 'select-student', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/timetableweek/timetableweek.module#TimetableweekPageModule', name: 'TimetableweekPage', segment: 'timetableweek', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/transport/transport.module#TransportPageModule', name: 'TransportPage', segment: 'transport', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_attendance_attendance__["a" /* AttendancePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_notice_board_notice_board__["a" /* NoticeBoardPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_annual_calendar_annual_calendar__["a" /* AnnualCalendarPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_events_events__["a" /* EventsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_homework_homework__["a" /* HomeworkPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_result_result__["a" /* ResultPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_select_student_select_student__["a" /* SelectStudentPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_transport_transport__["a" /* TransportPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_feedback_feedback__["a" /* FeedbackPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_timetableweek_timetableweek__["a" /* TimetableweekPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_day_time_table_day_time_table__["a" /* DayTimeTablePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\ionic\Idara\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"D:\ionic\Idara\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__select_student_select_student__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.password = '1234';
        this.email = 'snehal@gmail.com';
    }
    // ngOnInit(){
    //   this.formgroup = this.formBuilder.group({
    //       email: new FormControl('', Validators.compose([
    //       Validators.required,
    //       Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    //     ]))
    //   });
    // }
    LoginPage.prototype.login = function () {
        if (this.email == 'snehal@gmail.com' && this.password == '1234') {
            console.log('Welcome to new project');
            window.localStorage.setItem('id', '1');
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__select_student_select_student__["a" /* SelectStudentPage */]);
        }
        else {
            console.log('Invalid User name and password');
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\login\login.html"*/'\n<ion-content padding class="appcolor">\n\n<!-- <form [formGroup]="formgroup"> -->\n  <ion-grid>\n    <ion-list>\n      <ion-item class="logo">\n        <img src="assets/imgs/MSB-logo.png" class="imglogo">\n      </ion-item>\n\n    <div class="divpadding">\n      <ion-item class="borderradius">\n      <ion-label>E-mail</ion-label>\n      <ion-input type="email" [(ngModel)]="email" ></ion-input>\n      </ion-item>\n    </div>\n\n    <div class="divpadding">\n      <ion-item class="borderradius">\n      <ion-label>Password</ion-label>\n      <ion-input type="password" [(ngModel)]="password"></ion-input>\n      </ion-item>\n    </div>\n  </ion-list>\n  \n    <div class="divpadding">\n      <button ion-button class="center" color="light" (click)="login()">Login</button>\n    </div>\n  </ion-grid>\n<!-- </form> -->\n</ion-content>\n'/*ion-inline-end:"D:\ionic\Idara\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectStudentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SelectStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SelectStudentPage = /** @class */ (function () {
    function SelectStudentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.genderSelection = '0';
    }
    SelectStudentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SelectStudentPage');
    };
    SelectStudentPage.prototype.boyselect = function () {
        this.imglayoutchange1 = "imglayoutborder";
        this.imglayoutchange2 = "";
        this.genderSelection = 'boy';
    };
    SelectStudentPage.prototype.girlselect = function () {
        this.imglayoutchange2 = "imglayoutborder";
        this.imglayoutchange1 = "";
        this.genderSelection = 'girl';
    };
    SelectStudentPage.prototype.select = function () {
        if (this.genderSelection == "boy") {
        }
        else if (this.genderSelection == "girl") {
        }
        else {
        }
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    SelectStudentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-select-student',template:/*ion-inline-start:"D:\ionic\Idara\src\pages\select-student\select-student.html"*/'\n\n<ion-content class="appcolor">\n  <div>\n    <ion-label class="fontstyle">Select Student</ion-label>\n  </div>\n  <div>\n  <ion-grid class="griddesign">\n    <ion-row>\n      <ion-col class="itemset {{imglayoutchange1}}" (click)="boyselect()">\n        <img class="imglayout" src="assets/icon/boy.png"/>\n      </ion-col>\n     \n      <ion-col class="itemset {{imglayoutchange2}}" (click)="girlselect()">\n        <img class="imglayout" src="assets/icon/girl.png"/>\n      </ion-col>\n    </ion-row>\n    <ion-row>  \n      <ion-col>\n        <ion-label class="snamefont" text-wrap>Ali Mujjafar Shekh Hakim Kiranawala</ion-label>\n      </ion-col>\n      <ion-col>\n        <ion-label class="snamefont" text-wrap>Ali Mujjafar Shekh Hakim Kiranawala</ion-label>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n<div>\n    <button ion-button class="btndesign" block (click)="select()">Select</button>\n</div>\n</ion-content>'/*ion-inline-end:"D:\ionic\Idara\src\pages\select-student\select-student.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], SelectStudentPage);
    return SelectStudentPage;
}());

//# sourceMappingURL=select-student.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map