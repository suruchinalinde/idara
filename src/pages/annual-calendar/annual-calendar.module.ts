import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnualCalendarPage } from './annual-calendar';

@NgModule({
  declarations: [
    AnnualCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(AnnualCalendarPage),
  ],
})
export class AnnualCalendarPageModule {}
