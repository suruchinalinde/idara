import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { AttendancePage } from '../attendance/attendance';
import { HomeworkPage } from '../homework/homework';
import { ResultPage } from '../result/result';
import { TransportPage } from '../transport/transport';
import { NoticeBoardPage } from '../notice-board/notice-board';
import { FeedbackPage } from '../feedback/feedback';
import { AnnualCalendarPage } from '../annual-calendar/annual-calendar';
import { SelectStudentPage } from '../select-student/select-student';
import { ChangePasswordPage } from '../change-password/change-password';
import { LoginPage } from '../login/login';
import { TimetableweekPage } from '../timetableweek/timetableweek';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  public profile(){
    this.navCtrl.push(ProfilePage);
  }
  public attendance(){
    this.navCtrl.push(AttendancePage);
  }
  public homework(){
    this.navCtrl.push(HomeworkPage);
  }
  public marks(){
    this.navCtrl.push(ResultPage);
  }
  public buslocation(){
    this.navCtrl.push(TransportPage);
  }
  public noticeboard(){
    this.navCtrl.push(NoticeBoardPage);
  }
  public writetoschool(){
    this.navCtrl.push(FeedbackPage);
  }
  public timeTable(){
    this.navCtrl.push(TimetableweekPage);
  }
  public annualcalender(){
    this.navCtrl.push(AnnualCalendarPage);
  }

  SelectPage(menu){
    console.log(menu);
    if(menu==1){
      this.navCtrl.setRoot(SelectStudentPage);
    }
    else if(menu==2){
      this.navCtrl.push(ChangePasswordPage);
    }
else{
  this.navCtrl.setRoot(LoginPage);
  
    }
  }

}
