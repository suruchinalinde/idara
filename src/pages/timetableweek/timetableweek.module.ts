import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimetableweekPage } from './timetableweek';

@NgModule({
  declarations: [
    TimetableweekPage,
  ],
  imports: [
    IonicPageModule.forChild(TimetableweekPage),
  ],
})
export class TimetableweekPageModule {}
