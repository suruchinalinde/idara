import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SelectStudentPage } from '../select-student/select-student';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  password: string= '1234';
  email: any= 'snehal@gmail.com';
  formgroup: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
  }
  // ngOnInit(){
  //   this.formgroup = this.formBuilder.group({
  //       email: new FormControl('', Validators.compose([
  //       Validators.required,
  //       Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
  //     ]))
  //   });
  // }
  login(){
    
    if(this.email=='snehal@gmail.com' && this.password=='1234'){

      console.log('Welcome to new project');
      window.localStorage.setItem('id','1');
      this.navCtrl.push(SelectStudentPage);
    }
    else{
      console.log('Invalid User name and password');
    }
  }
    
}
