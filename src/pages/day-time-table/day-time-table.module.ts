import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayTimeTablePage } from './day-time-table';

@NgModule({
  declarations: [
    DayTimeTablePage,
  ],
  imports: [
    IonicPageModule.forChild(DayTimeTablePage),
  ],
})
export class DayTimeTablePageModule {}
